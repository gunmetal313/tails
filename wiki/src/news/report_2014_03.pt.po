# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-04-12 19:03+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails report for March, 2014\"]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Releases\n"
msgstr ""

#. type: Plain text
msgid "Tails 0.23 was released on March 19."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Metrics\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Tails has been started more than 261 878 times in March.  This make 8 448 "
"boots a day in average."
msgstr ""

#. type: Bullet: '- '
msgid "19 076 downloads of the OpenPGP signature of Tails ISO."
msgstr ""

#. type: Bullet: '- '
msgid "80 reports were received through WhisperBack."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Code\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"The Cookie Monster extension was [[!tails_gitweb_branch feature/6790-remove-"
"cookie-monster desc=\"removed from our browser\"]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!tails_ticket 6790]]\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"The new support for bridges was [[!tails_gitweb_branch feature/bridge-mode "
"desc=\"completed\"]]. It now uses Tor Launcher to configure Tor when needed."
msgstr ""

#. type: Bullet: '- '
msgid ""
"A bookmark to the Tor Stack Exchange was [[!tails_gitweb_commit "
"dd3604cb96039874f76ac335c62470752687f447 desc=\"added to our browser\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"A launcher for our documentation was [[!tails_gitweb_branch bugfix/tails-"
"documentation-launcher desc=\"added to the Tails menu\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Some race conditions when starting automatic upgrades were [[!"
"tails_gitweb_branch bugfix/6592-fix-races-with-check-for-upgrades desc="
"\"fixed\"]]. [[!tails_ticket 6592]]"
msgstr ""

#. type: Bullet: '- '
msgid ""
"The wikileaks.de server was [[!tails_gitweb_commit "
"5762c9dbffef63a0fc4634c8cff2c7396032ecd7 desc=\"removed from Pidgin\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"The #tor IRC channel was [[!tails_gitweb_commit "
"0910dee7e1dbf873a4d7d4650b0e5d5b315eeb2f desc=\"removed from Pidgin\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We [[!tails_gitweb_branch feature/better-iso-cleanup desc=\"cleaned up a bit "
"our ISO images\"]] to make them a bit smaller."
msgstr ""

#. type: Bullet: '- '
msgid ""
"A bug in the localization of Tor Launcher was [[!tails_gitweb_branch "
"bugfix/6885-tor-launcher-localisation desc=\"fixed\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We now [[!tails_gitweb_branch feature/linux-3.12 desc=\"install Linux "
"3.12\"]] from deb.tails.boum.org not to install Linux 3.13.5-1 from testing "
"yet."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Some work as been done towards [[!tails_gitweb_branch feature/wheezy desc="
"\"Tails Wheezy\"]], for example on the menus and launchers, the integration "
"of Seahorse Nautilus, network-manager, and the migration from GConf to dconf."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Documentation and website\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"A new contribute [[page|contribute/how/sysadmin]] for sysadmins to improve "
"the infrastructure behind Tails was [[!tails_gitweb_branch doc/6182-"
"contribute-how-sysadmin desc=\"written\"]]."
msgstr ""

#. type: Plain text
msgid "- Our sample `rtorrent.rc` was improved. [[!tails_ticket 6995]]"
msgstr ""

#. type: Bullet: '- '
msgid ""
"A sample configuration for Tails mirror using nginx was [[!"
"tails_gitweb_commit 4ea4c3e desc=\"added\"]]. [[!tails_ticket 6993]]"
msgstr ""

#. type: Bullet: '- '
msgid ""
"A \"News\" link was [[!tails_gitweb_commit "
"f85563928fedfefb3878cab96d5288ffdbf57b0c desc=\"added to the sidebar\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"The expectations for our mirrors in terms of bandwidth were [[!"
"tails_gitweb_commit 3c434520d7462b6210c262bcdb3620b0a4467cf7 desc=\"clarified"
"\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"The instructions to securely delete the persistent volume were [[!"
"tails_gitweb_commit adae16ddf1727b80ddfdd6d9c505b87846255138 desc=\"improved"
"\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"The instructions to manually backup the persistent volume were [[!"
"tails_gitweb_commit 4074e297bc9c83984270aacd4e4f1c70dd338834 desc=\"fixed"
"\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"gpgApplet was [[!tails_gitweb_commit "
"f4bf2155d635400e5dfe969996a62989c8af67b7 desc=\"renamed\"]] \"Tails OpenPGP "
"Applet\"."
msgstr ""

#. type: Plain text
msgid ""
"- \"Tails browser\" was consistently renamed \"Tor browser\". [[!"
"tails_ticket 6574]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"- Our list of related projects was updated:\n"
"  - [Freepto](http://www.freepto.mx/) was added.\n"
"  - [Liberté Linux](http://dee.su/liberte) is in a sleeping state.\n"
"  - [uVirtus](http://uvirtus.org/) is abandoned.\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"The content of our Troubleshooting page was merging into the main "
"[[Support]] page."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Email commands to interact with Redmine were [[!tails_gitweb_commit "
"c0cd62c46f8ef1d4682c1a72b80ed45b21083578 desc=\"documented\"]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"The documentation for MAC spoofing was [[completed|doc/first_steps/"
"startup_options/mac_spoofing]]."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Translation\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"The German team which started to work earlier this year, got more volunteers "
"on board and more work done."
msgstr ""

#. type: Bullet: '- '
msgid ""
"People volunteered on the tails-l10n mailing-list to translate our website "
"into Italian, Spanish, Turkish, and Portuguese (Portugal)."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Infrastructure\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"The mailing-list for early testers, tails-testers@boum.org, was [[announced|"
"news/tails-testers]] and already joined by 35 people."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We are ready to welcome new contributors to [[improve the infrastructure "
"behind Tails|contribute/how/sysadmin]], and drafted a call on that topic."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"- Three branches were merged to improve our automated test suite:\n"
"  - [[!tails_gitweb_branch test/5959-antitest-memory-erasure]]\n"
"  - [[!tails_gitweb_branch test/sniffer-vs-mac-spoof]]\n"
"  - [[!tails_gitweb_branch test/tor-is-ready-notification]]\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Our manual test suite for the browser [[!tails_gitweb_commit "
"e1845741a11cfaace228b79778a5a01a505fe871 desc=\"was reorganized\"]] to "
"better match the tests conducted for the TBB."
msgstr ""

#. type: Title =
#, no-wrap
msgid "On-going discussions\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"[SHA256sum replacement or patch](https://mailman.boum.org/pipermail/tails-"
"dev/2014-March/005186.html), about a possible replacement for our hash "
"extension for Firefox."
msgstr ""

#. type: Bullet: '- '
msgid ""
"[Feedback wanted on planned implementation of Feature #5301 - Clone or "
"Backup Persistent Volume](https://mailman.boum.org/pipermail/tails-dev/2014-"
"March/005225.html)"
msgstr ""

#. type: Bullet: '- '
msgid ""
"[Testing EHLO messages: simplification proposal](https://mailman.boum.org/"
"pipermail/tails-dev/2014-March/005227.html)"
msgstr ""

#. type: Bullet: '- '
msgid ""
"[Perspectives for 0.23..1.1](https://mailman.boum.org/pipermail/tails-"
"dev/2014-March/005234.html)"
msgstr ""

#. type: Bullet: '- '
msgid ""
"[Upgrading the Linux kernel for 1.0?](https://mailman.boum.org/pipermail/"
"tails-dev/2014-March/005237.html)"
msgstr ""

#. type: Bullet: '- '
msgid ""
"[Review of https://tails.boum.org/contribute/index.en.html](https://mailman."
"boum.org/pipermail/tails-dev/2014-March/005298.html), about restructuring "
"the links on the Contribute page."
msgstr ""

#. type: Bullet: '- '
msgid ""
"[More tails.boum.org HTTP response headers?](https://mailman.boum.org/"
"pipermail/tails-dev/2014-March/005278.html)"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Funding\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"We were [awarded](https://twitter.com/accessnow/status/441043400708857856) "
"the [Access Innovation Prize](https://www.accessnow.org/prize)."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We submitted a proposal to the Knight Foundation [News Challenge](https://"
"www.newschallenge.org/challenge/2014/submissions/improve-tails-to-limit-the-"
"impact-of-security-flaws-isolate-critical-applications-and-provide-same-day-"
"security-updates)."
msgstr ""

#. type: Bullet: '- '
msgid ""
"The [crowdfunding campaign](https://pressfreedomfoundation.org/) by the "
"Freedom of the Press Foundation was extended and revived thanks to [some new]"
"(https://pressfreedomfoundation.org/blog/2014/02/why-its-vital-public-fund-"
"open-source-encryption-tools)  [articles](https://pressfreedomfoundation.org/"
"blog/2014/04/help-support-little-known-privacy-tool-has-been-critical-"
"journalists-reporting-nsa)  by Trevor Timm."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We applied to be audited through the [OpenITP Peer Review Board](https://prb."
"openitp.org)."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We are still the process of signing the contract for a grant we were awarded "
"by OpenITP at the end of 2013."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Outreach\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"We organized a [[news/logo_contest]] to have a new logo in time for Tails "
"1.0. We received [[36 proposals|blueprint/logo]]! Tails contributors started "
"to vote on their favourite proposals."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We scheduled two usability testing sessions together with [Silicon Sentier]"
"(http://siliconsentier.org/) on May 21 and May 28 in Paris."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We scheduled a public hackfest. It will take place at [IRILL](http://www."
"irill.org/) (Paris, France) on July 5 and 6."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We made progress on the organization of the 2014 edition of the yearly Tails "
"contributors summit. We could make use of more funding sources."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Press and testimonials\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"2014-03-17: In [Index Freedom of Expression Awards: Digital activism nominee "
"Tails](http://www.indexoncensorship.org/2014/03/index-freedom-expression-"
"awards-digital-activism-nominee-tails/), Alice Kirkland interviews the Tails "
"project about our nomination for the *Censorship’s Freedom of Expression "
"Awards*."
msgstr ""

#. type: Bullet: '* '
msgid ""
"2014-03-13: In his [Les 7 clés pour protéger ses communications](http://www."
"tdg.ch/high-tech/web/Les-7-cles-pour-proteger-ses-communications/"
"story/25588689)  article (in French) published by the *Tribune de Genève*, "
"Simon Koch recommends using Tails."
msgstr ""

#. type: Bullet: '* '
msgid ""
"2014-03-12: In his [Happy 25th Birthday World Wide Web - Let's Not Destroy "
"It](http://www.huffingtonpost.co.uk/mike-harris/world-wide-web_b_4947687."
"html?utm_hp_ref=uk)  article published by the Huffington Post, Mike Harris "
"writes that \"Increasing numbers of activists are using high-tech tools such "
"as Tor or Tails to encrypt their internet browsing and email\"."
msgstr ""

#. type: Bullet: '* '
msgid ""
"2014-03-12: In his [US and UK Spy Agencies Are \"Enemies of the Internet\"]"
"(http://motherboard.vice.com/read/us-and-uk-spy-agencies-are-enemies-of-the-"
"internet)  article, published in the Motherboard section of the Vice "
"network, Joseph Cox covers Reporters Without Borders' [latest report]"
"(https://en.rsf.org/enemies-of-the-internet-2014-11-03-2014,45985.html), and "
"writes \"If you're a journalist working on anything more sensitive than "
"London Fashion Week or League 2 football, you might want to consider using "
"the Linux-based 'Tails' operating system too.\""
msgstr ""

#. type: Bullet: '* '
msgid ""
"2014-03-08: [Reporters Without Borders](http://en.rsf.org/)'s Grégoire "
"Pouget blogs about Tails: [FIC 2014 : Comment être réellement anonyme sur "
"Internet](http://blog.barbayellow.com/2014/03/08/fic-2014-comment-etre-"
"reellement-anonyme-sur-internet/)  (in French)."
msgstr ""

#. type: Bullet: '* '
msgid ""
"2014-03-03: In the March edition of the Linux Journal, that [celebrates 20 "
"years of this journal](http://www.linuxjournal.com/content/march-2014-issue-"
"linux-journal-20-years-linux-journal), Kyle demonstrates Tails."
msgstr ""
